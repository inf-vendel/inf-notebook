FROM python:3.9-slim-buster

LABEL Name="Inf-notebook" Version=1.1

ARG srcDir=app
WORKDIR /app
COPY $srcDir/requirements.txt .
RUN pip install -r requirements.txt

COPY $srcDir/ .

ENV PORT 5000
EXPOSE 5000
ENTRYPOINT [ "python" ]
CMD [ "server.py" ]

