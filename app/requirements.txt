Flask==2.0.2
Flask-PyMongo
itsdangerous==2.0.1
Jinja2==3.0.3
MarkupSafe==2.0.1
Werkzeug==2.0.2
markdown==3.3.7
gunicorn==20.1.0
